/**
 * @file
 * The JS implementing the web worker functionality.
 */

onmessage = function (e) {
  'use strict';

  // The passed-in data is available via e.data.
  var url = '/activity_ping?path=' + e.data.path + '&overview=' + e.data.overview + '&title=' + e.data.title;
  ping(url, e.data.interval);
};


// Simple XHR request in pure JavaScript.
function load(url, callback) {
  'use strict';
  var xhr;

  if (typeof XMLHttpRequest !== 'undefined') {
    xhr = new XMLHttpRequest();
  }
  else {
    var versions = ['MSXML2.XmlHttp.5.0',
        'MSXML2.XmlHttp.4.0',
        'MSXML2.XmlHttp.3.0',
        'MSXML2.XmlHttp.2.0',
        'Microsoft.XmlHttp'];

    for (var i = 0, len = versions.length; i < len; i++) {
      try {
        xhr = new ActiveXObject(versions[i]);
        break;
      }
      catch (e) {
        // Do nothing.
      }
    }
  }

  xhr.onreadystatechange = ensureReadiness;

  function ensureReadiness() {
    if (xhr.readyState < 4) {
      return;
    }

    if (xhr.status !== 200) {
      return;
    }

    // All is well.
    if (xhr.readyState === 4) {
      callback(xhr);
    }
  }

  xhr.open('GET', url, true);
  xhr.send('');
}

function ping(url, interval) {
  'use strict';
  load(url, function (xhr) {
    var result = xhr.responseText;
    postMessage(result);
  });

  setTimeout(function () {
    ping(url, interval);
  }, interval);
}
