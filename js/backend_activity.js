/**
 * @file
 * Initializes the web worker thread.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.backend_activity = {
    attach: function (context, settings) {
      var w;
      var isOverviewPage = 0;

      if (window.location.pathname === '/admin/reports/backend_activity') {
        isOverviewPage = 1;
      }
      if (typeof (Worker) !== 'undefined') {
        if (typeof (w) == 'undefined') {
          w = new Worker('/' + Drupal.settings.backend_activity_module_path + '/js/worker.js');
          w.postMessage({path: window.location.pathname, title: Drupal.settings.backend_activity_page_title, overview: isOverviewPage, interval: Drupal.settings.backend_activity_ping_interval});
        }
        w.onmessage = function (event) {
          if (event.data === 'HIT_LOCK') {
            // Don't do anything, as we didn't actually get any update information.
          }
          else {
            document.getElementById('backend-activity--messages').innerHTML = event.data;
          }
        };
      }
      else {
        // No Web Worker support.
      }
    }
  };
})(jQuery);
