<?php

/**
 * @file
 * Administration functions for the backend activity module.
 */

/**
 * The admin form for the module.
 */
function backend_activity_admin_form() {
  $form = array();

  $form['backend_activity_ping_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Ping interval (in ms)'),
    '#default_value' => variable_get('backend_activity_ping_interval', BACKEND_ACTIVITY_MINIMUM_PING_INTERVAL),
    '#description' => t('Every how many seconds should an open browser tab try to send a ping to the backend? The lower the number the more requests will be fired. If you have many users you might want to try a higher number. Please also be aware that not every ping request is guaranteed to be handled by the backend as only one request can be handled at any given time.'),
  );

  $form['backend_activity_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeout (in ms)'),
    '#default_value' => variable_get('backend_activity_timeout', BACKEND_ACTIVITY_MINIMUM_TIMEOUT),
    '#description' => t('After how many seconds of not hearing back from an open browser tab should the module assume that the user has closed the tab? It should definitely be a multiple of the set interval above, as not every ping request is guaranteed to get through.'),
  );
  return system_settings_form($form);
}

/**
 * Some validation to prevent stupid things from happening.
 */
function backend_activity_admin_form_validate($form, &$form_state) {
  if (intval($form_state['values']['backend_activity_ping_interval']) < BACKEND_ACTIVITY_MINIMUM_PING_INTERVAL) {
    form_set_error('backend_activity_ping_interval', t('Minimum ping interval is !MINIMUM_PING_INTERVAL.', array('!MINIMUM_PING_INTERVAL' => BACKEND_ACTIVITY_MINIMUM_PING_INTERVAL)));
  }
  if (intval($form_state['values']['backend_activity_timeout']) < BACKEND_ACTIVITY_MINIMUM_TIMEOUT) {
    form_set_error('backend_activity_timeout', t('Minimum timeout is !MINIMUM_TIMEOUT.', array('!MINIMUM_TIMEOUT' => BACKEND_ACTIVITY_MINIMUM_TIMEOUT)));
  }
}
