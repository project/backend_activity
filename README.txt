
Backend activity module

PREREQUISITES

Drupal 7.x

OVERVIEW

Concurrent content authoring in Drupal can be quite painful,
when you notice, that you can't save your last 10 minutes changes
because somebody else saved a resource in between and your current
form gets invalidated.

INSTALLATION

Install the module and place the block "Backend activity messages"
somewhere at the top of the content region of the admin theme.
This block will show warning messages, if another user visits the
current backend URL path the module will show a warning. You should make
sure that you only put this block onto URL paths which are prone to
problems with concurrent editing like node edit forms etc.

Here are some URL patterns with wildcards for copy & pasting:
node/*/edit
admin/structure/types/manage/*

ADMINISTRATOR USAGE

Visit Administer >> Reports >> Backend activity overview to access 
a full overview list of current backend activity. 

AUTHOR

Stefan Lehmann
